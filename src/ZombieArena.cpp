#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <sstream>
#include <fstream>
#include "Player.h"
#include "ZombieArena.hpp"
#include "TextureHolder.hpp"
#include "Bullet.hpp"
#include "Pickup.hpp"

using namespace sf;

int main()
{
	TextureHolder texHolder;

	// Start with the GAME_OVER state
	State state = State::GAME_OVER; //Debug affichage : mettre PLAYING au lieu de GAME_OVER

	// Get the screen resolution and create an SFML window
	Vector2f resolution;
	resolution.x = VideoMode::getDesktopMode().width;
	resolution.y = VideoMode::getDesktopMode().height;

	RenderWindow window(VideoMode(resolution.x, resolution.y), 
		"Zombie Arena", Style::Fullscreen);

	// Create a an SFML View for the main action
	View mainView(sf::FloatRect(0, 0, resolution.x, resolution.y));

	// Here is our clock for timing everything
	Clock clock;
	// How long has the PLAYING state been active
	Time gameTimeTotal;

	// Where is the mouse in relation to world coordinates
	Vector2f mouseWorldPosition;
	// Where is the mouse in relation to screen coordinates
	Vector2i mouseScreenPosition;

	// Create an instance of the Player class
	Player player;

	// The boundaries of the arena
	IntRect arena;

    Texture tex_bg = TextureHolder::GetTexture(DIRECTORY+"background_sheet.png");
	VertexArray background;

	//Zombie horde
	int numZombies = 0, numZombiesAlive = 0;
	Zombie* zombies = nullptr;

    //BUllets
    Bullet bullets[100];
    int currentBullet = 0;
    int bulletsSpare = 24;
    int bulletsInClip = 6;
    int clipSize = 6;
    float fireRate = 1;
    Time lastPressed;

    //Crosshair
    window.setMouseCursorVisible(false);
    Sprite spr_Crosshair;
    Texture tex_Crosshair = TextureHolder::GetTexture(DIRECTORY+"crosshair.png");
    spr_Crosshair.setTexture(tex_Crosshair);
    spr_Crosshair.setOrigin(25, 25);

    Pickup healthPickup(0);
    Pickup ammoPickup(1);

    //About game
    unsigned int score = 0;
    unsigned int hiScore = 0;

    //Home/game over screen
    Sprite sprGO;
    sprGO.setTexture(TextureHolder::GetTexture(DIRECTORY+"background.png"));
    sprGO.setPosition(0,0);

    //HUD
    View HUDView(sf::FloatRect(0,0,resolution.x,resolution.y));

    Sprite spr_AmmoIcon;
    spr_AmmoIcon.setTexture(TextureHolder::GetTexture(DIRECTORY+"ammo_icon.png"));
    spr_AmmoIcon.setPosition(20, 980);

    Font font;
    font.loadFromFile("../fonts/zombiecontrol.ttf");

    Text pausedText;
    pausedText.setFont(font);
    pausedText.setCharacterSize(155);
    pausedText.setColor(Color::White);
    pausedText.setPosition(400, 400);
    pausedText.setString("Press Enter \n to resume");

    Text goText;
    goText.setFont(font);
    goText.setCharacterSize(125);
    goText.setColor(Color::White);
    goText.setPosition(250, 850);
    goText.setString("Press Enter to play");

    Text lvlUpText;
    lvlUpText.setFont(font);
    lvlUpText.setCharacterSize(80);
    lvlUpText.setColor(Color::White);
    lvlUpText.setPosition(150, 250);
    stringstream lvlUpStream;
    lvlUpStream <<    "1 - Increased rate of fire" <<
                    "\n2 - Increased clip size" <<
                    "\n3 - Increased max health" <<
                    "\n4 - Increased run speed" <<
                    "\n5 - More and better health pickups" <<
                    "\n6 - More and better ammo pickups";
    lvlUpText.setString(lvlUpStream.str());

    Text ammoText;
    ammoText.setFont(font);
    ammoText.setCharacterSize(55);
    ammoText.setColor(Color::White);
    ammoText.setPosition(200, 980);

    Text scoreText;
    scoreText.setFont(font);
    scoreText.setCharacterSize(55);
    scoreText.setColor(Color::White);
    scoreText.setPosition(20, 0);

    ifstream hiscoreFile("../gamedata/score.txt");
    if (hiscoreFile.is_open())
    {
        hiscoreFile >> hiScore;
        hiscoreFile.close();
    }

    Text hiScoreText;
    hiScoreText.setFont(font);
    hiScoreText.setCharacterSize(55);
    hiScoreText.setColor(Color::White);
    hiScoreText.setPosition(1400, 0);
    stringstream ssHi;
    ssHi << "Hi Score : " << hiScore;

    Text zombiesRemainingText;
    zombiesRemainingText.setFont(font);
    zombiesRemainingText.setCharacterSize(55);
    zombiesRemainingText.setColor(Color::White);
    zombiesRemainingText.setPosition(1500, 980);
    zombiesRemainingText.setString("Zombies : 100");

    int wave = 0;
    Text waveText;
    waveText.setFont(font);
    waveText.setCharacterSize(55);
    waveText.setColor(Color::White);
    waveText.setPosition(1250, 980);
    waveText.setString("Wave : 0");

    //Health bar
    RectangleShape hpBar;
    hpBar.setFillColor(Color::Green);
    hpBar.setPosition(450, 980);

    int framesSinceLastHUDUpdate = 0;

    int fpsMeasurementFrameInterval = 100;

    //The sound
    string soundDir = "../sound/";

    SoundBuffer hitBuffer;
    hitBuffer.loadFromFile(soundDir+"hit.wav");
    Sound snd_hit;
    snd_hit.setBuffer(hitBuffer);

    SoundBuffer splatBuffer;
    splatBuffer.loadFromFile(soundDir+"splat.wav");
    Sound snd_splat;
    snd_splat.setBuffer(splatBuffer);

    SoundBuffer shootBuffer;
    shootBuffer.loadFromFile(soundDir+"shoot.wav");
    Sound snd_shoot;
    snd_shoot.setBuffer(shootBuffer);

    SoundBuffer rldBuffer;
    rldBuffer.loadFromFile(soundDir+"reload.wav");
    Sound snd_reload;
    snd_reload.setBuffer(rldBuffer);

    SoundBuffer rldFailBuffer;
    rldFailBuffer.loadFromFile(soundDir+"reload_failed.wav");
    Sound snd_rldFail;
    snd_rldFail.setBuffer(rldFailBuffer);

    SoundBuffer powerUpBuffer;
    powerUpBuffer.loadFromFile(soundDir+"powerup.wav");
    Sound snd_powerUp;
    snd_powerUp.setBuffer(powerUpBuffer);

    SoundBuffer pickUpBuffer;
    pickUpBuffer.loadFromFile(soundDir+"pickup.wav");
    Sound snd_pickUp;
    snd_pickUp.setBuffer(pickUpBuffer);

	// The main game loop
	while (window.isOpen())
	{
		/*
		************
		Handle input
		************
		*/

		// Handle events
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::KeyPressed)
			{									
				// Pause a game while playing
				if (event.key.code == Keyboard::Return &&
					state == State::PLAYING)
				{
					state = State::PAUSED;
				}

				// Restart while paused
				else if (event.key.code == Keyboard::Return &&
					state == State::PAUSED)
				{
					state = State::PLAYING;
					// Reset the clock so there isn't a frame jump
					clock.restart();
				}

				// Start a new game while in GAME_OVER state
				else if (event.key.code == Keyboard::Return &&
					state == State::GAME_OVER)
				{
					state = State::LEVELING_UP;
                    wave = 0;
                    score = 0;

                    currentBullet = 0;
                    bulletsSpare = 24;
                    bulletsInClip = 6;
                    clipSize = 6;
                    fireRate = 1;

                    player.resetPlayerStats();
				}

				if (state == State::PLAYING)
				{
                    if (event.key.code == Keyboard::R) //Reloading
                    {
                        if (bulletsSpare >= clipSize)
                        {
                            bulletsInClip = clipSize;
                            bulletsSpare -= clipSize;
                            snd_reload.play();
                        }
                        else if (bulletsSpare > 0)
                        {
                            bulletsInClip = bulletsSpare;
                            bulletsSpare = 0;
                            snd_reload.play();
                        }
                        else
                        {
                            snd_rldFail.play();
                        }

                    }

				}

			}
		}// End event polling


		// Handle the player quitting
		if (Keyboard::isKeyPressed(Keyboard::Escape))
		{
			window.close();
		}

		// Handle controls while playing
		if (state == State::PLAYING)
		{
			// Handle the pressing and releasing of the WASD keys
			if (Keyboard::isKeyPressed(Keyboard::Z))
			{
				player.moveUp();
			}
			else
			{
				player.stopUp();
			}

			if (Keyboard::isKeyPressed(Keyboard::S))
			{
				player.moveDown();
			}
			else
			{
				player.stopDown();
			}

			if (Keyboard::isKeyPressed(Keyboard::Q))
			{
				player.moveLeft();
			}
			else
			{
				player.stopLeft();
			}

			if (Keyboard::isKeyPressed(Keyboard::D))
			{
				player.moveRight();
			}
			else
			{
				player.stopRight();
			}

            if (Mouse::isButtonPressed(sf::Mouse::Left))
            {
                if (gameTimeTotal.asMilliseconds() - lastPressed.asMilliseconds() > 1000 / fireRate && bulletsInClip > 0)
                {
                    bullets[currentBullet++].shoot(player.getCenter().x, player.getCenter().y, mouseWorldPosition.x, mouseWorldPosition.y);

                    currentBullet %= 100;
                    lastPressed = gameTimeTotal;
                    --bulletsInClip;

                    snd_shoot.play();
                }
            }

		}// End WASD while playing

		// Handle the levelling up state
		if (state == State::LEVELING_UP)
		{
			// Handle the player levelling up
			if (event.key.code == Keyboard::Num1)
			{
				state = State::PLAYING;
                ++fireRate;
			}

			if (event.key.code == Keyboard::Num2)
			{
				state = State::PLAYING;
                clipSize *= 2;
			}

			if (event.key.code == Keyboard::Num3)
			{
				state = State::PLAYING;
                player.upgradeHealth();
			}

			if (event.key.code == Keyboard::Num4)
			{
				state = State::PLAYING;
                player.upgradeSpeed();
			}

			if (event.key.code == Keyboard::Num5)
			{
				state = State::PLAYING;
                healthPickup.upgrade();
			}

			if (event.key.code == Keyboard::Num6)
			{
				state = State::PLAYING;
                ammoPickup.upgrade();
			}
			
			if (state == State::PLAYING)
			{
                ++wave;

				// Prepare the level
				// We will modify the next two lines later
				arena.width = 500 * wave;
				arena.height = 500 * wave;
				arena.left = 0;
				arena.top = 0;

				int tileSize = createBackground(background, arena);

				// Spawn the player in the middle of the arena
				player.spawn(arena, resolution, tileSize);

                //Pickup configuration
                healthPickup.setArena(arena);
                ammoPickup.setArena(arena);

				//Create zombie horde
				numZombies = 5 * wave;

				//Cleaning previous
				delete[] zombies;
				zombies = createHorde(numZombies, arena);
				numZombiesAlive = numZombies;

                snd_powerUp.play();
				
				// Reset the clock so there isn't a frame jump
				clock.restart();
			}
		}// End levelling up

		/*
		****************
		UPDATE THE FRAME
		****************
		*/
		if (state == State::PLAYING)
		{
			// Update the delta time
			Time dt = clock.restart();
			// Update the total game time
			gameTimeTotal += dt;
			// Make a decimal fraction of 1 from the delta time
			float dtAsSeconds = dt.asSeconds();

			// Where is the mouse pointer
			mouseScreenPosition = Mouse::getPosition();

			// Convert mouse position to world coordinates of mainView
			mouseWorldPosition = window.mapPixelToCoords(
				Mouse::getPosition(), mainView);

            //Crosshair to the cursor
            spr_Crosshair.setPosition(mouseWorldPosition);

			// Update the player
			player.update(dtAsSeconds, Mouse::getPosition());

			// Make a note of the players new position
			Vector2f playerPosition(player.getCenter());

			// Make the view centre around the player				
			mainView.setCenter(player.getCenter());

			//Drawing zombies
			#pragma omp parallel for
            for (int i = 0; i < numZombies; ++i)
			{
				if (zombies[i].isAlive())
					zombies[i].update(dt.asSeconds(), playerPosition);
			}

            #pragma omp parallel for
            for (int i = 0; i < 100; ++i)
            {
                if (bullets[i].isInFlight())
                    bullets[i].update(dtAsSeconds);
            }

            healthPickup.update(dtAsSeconds);
            ammoPickup.update(dtAsSeconds);

            //Collision detection
            //Detection bullets - zombies
            for (int i = 0; i < 100; ++i)
            {
                if (bullets[i].isInFlight())
                {
                    for (int j = 0; j < numZombies; ++j)
                    {
                        if (zombies[j].isAlive())
                        {
                            if (bullets[i].getPosition().intersects(zombies[j].getPosition()))
                            {
                                bullets[i].stop();

                                if (zombies[j].hit()) //Register hit && check if he dies
                                {
                                    score += 10;
                                    if (score >= hiScore)
                                        hiScore = score;

                                    --numZombiesAlive;

                                    if (numZombiesAlive <= 0)
                                        state = State::LEVELING_UP;
                                }

                                snd_splat.play();
                            }
                        }
                    }
                }
            } //End for collision detection bullets - zombies

            //Detection Player - pickups
            if(healthPickup.isSpawned())
                if (player.getPosition().intersects(healthPickup.getPosition())) {
                    player.increaseHealthLevel(healthPickup.gotIt());
                    snd_pickUp.play();
                }

            if (ammoPickup.isSpawned())
                if (player.getPosition().intersects(ammoPickup.getPosition())) {
                    bulletsSpare += ammoPickup.gotIt();
                    snd_reload.play();
                }


            //Detection Player - Zombie
            for (int i = 0; i < numZombies; ++i)
            {
                if (! zombies[i].isAlive())
                    continue;

                if (player.getPosition().intersects(zombies[i].getPosition()))
                {
                    if (player.hit(gameTimeTotal))
                    {
                        snd_hit.play();
                    }

                    if (player.getHealth() <= 0) {
                        state = State::GAME_OVER;

                        ofstream _hiscoreFile("../gamedata/score.txt");
                        _hiscoreFile << hiScore;
                        _hiscoreFile.close();
                    }
                }
            } //End detection player - zombie

            //HPBar update
            hpBar.setSize(Vector2f(300 * player.getPercentageHealth(), 50));

            //Change color according to remaining health
            if (player.getPercentageHealth() >= .66)
                hpBar.setFillColor(Color::Green);
            else if (player.getPercentageHealth() <= .33)
                hpBar.setFillColor(Color::Red);
            else
                hpBar.setFillColor(Color::Yellow);

            ++framesSinceLastHUDUpdate;

            if (framesSinceLastHUDUpdate > fpsMeasurementFrameInterval)
            {
                stringstream ssAmmo, ssScore, ssHiScore, ssWave, ssZombiesAlive;

                ssAmmo << bulletsInClip << "/" << bulletsSpare;
                ammoText.setString(ssAmmo.str());

                ssScore << "Score : " << score;
                scoreText.setString(ssScore.str());

                ssHiScore << "Hi Score : " << hiScore;
                hiScoreText.setString(ssHiScore.str());

                ssWave << "Wave : " << wave;
                waveText.setString(ssWave.str());

                ssZombiesAlive << "Zombies : " << numZombiesAlive;
                zombiesRemainingText.setString(ssZombiesAlive.str());

                framesSinceLastHUDUpdate = 0;
            }

		}// End updating the scene

		/*
		**************
		Draw the scene
		**************
		*/

		if (state == State::PLAYING)
		{
			window.clear();

			// set the mainView to be displayed in the window
			// And draw everything related to it
			window.setView(mainView);

			//Drawing background
			window.draw(background, &tex_bg);

			#pragma omp parallel for
			for (int i = 0; i < numZombies; ++i)
			{
				window.draw(zombies[i].getSprite());
			}

            #pragma omp parallel for
            for (int i = 0; i < 100; ++i)
                if (bullets[i].isInFlight())
                    window.draw(bullets[i].getShape());

			// Draw the player
			window.draw(player.getSprite());

            // Draw pickups
            if (ammoPickup.isSpawned())
                window.draw(ammoPickup.getSprite());

            if (healthPickup.isSpawned())
                window.draw(healthPickup.getSprite());

            // Draw cursor
            window.draw(spr_Crosshair);

            //Hud view
            window.setView(HUDView);

            window.draw(spr_AmmoIcon);
            window.draw(ammoText);
            window.draw(scoreText);
            window.draw(hiScoreText);
            window.draw(hpBar);
            window.draw(waveText);
            window.draw(zombiesRemainingText);
		}

		if (state == State::LEVELING_UP)
		{
            window.draw(sprGO);
            window.draw(lvlUpText);
		}

		if (state == State::PAUSED)
		{
            window.draw(pausedText);
		}

		if (state == State::GAME_OVER)
		{
            window.draw(sprGO);
            window.draw(goText);
            window.draw(scoreText);
            window.draw(hiScoreText);
		}

		window.display();

	}// End game loop

	delete[] zombies; //Cleaning at the end of the game

	return 0;
}
