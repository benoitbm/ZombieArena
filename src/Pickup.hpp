//
// Created by benoit on 02/11/17.
//

#ifndef ZOMBIEARENA_PICKUP_HPP
#define ZOMBIEARENA_PICKUP_HPP

#pragma once

#include <SFML/Graphics.hpp>

using namespace sf;

class Pickup {
private:
    const int HEALTH_START = 50,
              AMMO_START = 12,
              START_WAIT_TIME = 10,
              START_SECONDS_TO_LIVE = 5;

    Sprite m_Sprite;

    IntRect m_Arena;

    //Value of the pickup
    int m_Value;

    int m_Type; //0 = HP, 1 = ammo

    bool m_Spawned = false;
    float m_SecondsSinceSpawn;
    float m_SecondsSinceDespawn;
    float m_SecondsToLive;
    float m_SecondsToWait;

public:
    Pickup(int type);

    void setArena(IntRect arena);
    void spawn();

    inline FloatRect getPosition() { return m_Sprite.getGlobalBounds();}

    inline Sprite getSprite() { return m_Sprite;}

    void update(float elapsedTime);

    inline bool isSpawned() {return m_Spawned;}

    int gotIt();

    void upgrade();
};


#endif //ZOMBIEARENA_PICKUP_HPP
