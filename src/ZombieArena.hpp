#ifndef ZOMBIEARENA_ZOMBIEARENA_HPP
#define ZOMBIEARENA_ZOMBIEARENA_HPP

#pragma once

#include "Zombie.hpp"

using namespace sf;
using namespace std;

// The game will always be in one of four states
enum class State { PAUSED, LEVELING_UP, GAME_OVER, PLAYING };

int createBackground(VertexArray& rVA, IntRect arena);
Zombie* createHorde(int numZombies, IntRect arena);
#endif //ZOMBIEARENA_ZOMBIEARENA_HPP
