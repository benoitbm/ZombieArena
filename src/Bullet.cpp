//
// Created by bboyer02 on 27/10/17.
//

#include "Bullet.hpp"

using namespace sf;
using namespace std;

Bullet::Bullet() {
    m_BulletShape.setSize(Vector2f(2,2));
}

void Bullet::shoot(float startX, float startY, float targetX, float targetY) {
    m_InFlight = true;
    m_Position = Vector2f(startX, startY);

    float gradient = abs((startX - targetX) / (startY - targetY));

    float ratioXY = m_Speed / (1 + gradient);

    m_BulletDistY = ratioXY;
    m_BulletDistX = ratioXY * gradient;

    if (targetX < startX)
        m_BulletDistX *= -1;

    if (targetY < startY)
        m_BulletDistY *= -1;

    float range = 1000;
    m_MinX = startX - range;
    m_MaxX = startX + range;
    m_MinY = startY - range;
    m_MaxY = startY + range;

    m_BulletShape.setPosition(m_Position);
}

void Bullet::update(float elapsedTime) {
    m_Position.x += m_BulletDistX * elapsedTime;
    m_Position.y += m_BulletDistY * elapsedTime;

    m_BulletShape.setPosition(m_Position);

    m_InFlight = m_Position.x >= m_MinX && m_Position.x <= m_MaxX && m_Position.y >= m_MinY && m_Position.y <= m_MaxY;
}