//
// Created by bboyer02 on 27/10/17.
//

#ifndef ZOMBIEARENA_BULLET_HPP
#define ZOMBIEARENA_BULLET_HPP

#pragma once

#include <SFML/Graphics.hpp>

using namespace sf;

class Bullet {

private:
    Vector2f m_Position;

    RectangleShape m_BulletShape;

    bool m_InFlight = false;

    float m_Speed = 1000;

    float m_BulletDistX, m_BulletDistY;
    float m_MinX, m_MaxX, m_MinY, m_MaxY;

public:
    Bullet();

    inline void stop() {m_InFlight = false;}

    inline bool isInFlight() { return m_InFlight;}

    void shoot(float startX, float startY, float targetX, float targetY);

    inline FloatRect getPosition() { return  m_BulletShape.getGlobalBounds();}

    inline RectangleShape getShape() { return m_BulletShape;}

    void update(float elapsedTime);

};


#endif //ZOMBIEARENA_BULLET_HPP
