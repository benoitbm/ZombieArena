//
// Created by benoit on 26/10/17.
//

#include "Zombie.hpp"
#include "TextureHolder.hpp"

using namespace std;

void Zombie::spawn(float startX, float startY, int type, int seed) {

    switch(type)
    {
        case 0: //Bloater
            m_Sprite = Sprite(TextureHolder::GetTexture(DIRECTORY+"bloater.png"));
            m_Speed = BLOATER_SPEED;
            m_Health = BLOATER_HEALTH;
            break;

        case 1: //Chaser
            m_Sprite = Sprite(TextureHolder::GetTexture(DIRECTORY+"chaser.png"));
            m_Speed = CHASER_SPEED;
            m_Health = CHASER_HEALTH;
            break;

        case 2: //Crawler
            m_Sprite = Sprite(TextureHolder::GetTexture(DIRECTORY+"crawler.png"));
            m_Speed = CRAWLER_SPEED;
            m_Health = CRAWLER_HEALTH;
            break;
    }

    srand((int)time(0) * seed);

    float modifier = (rand() % MAX_VARIANCE) + OFFSET;
    modifier /= 100;

    m_Speed *= modifier;

    m_Position.x = startX; m_Position.y = startY;

    m_Sprite.setOrigin(25, 25);

    m_Sprite.setPosition(m_Position);
}

void Zombie::update(float elapsedTime, Vector2f playerPosition) {
    if (playerPosition.x > m_Position.x)
        m_Position.x += m_Speed * elapsedTime;
    else
        m_Position.x -= m_Speed * elapsedTime;

    if (playerPosition.y > m_Position.y)
        m_Position.y += m_Speed * elapsedTime;
    else
        m_Position.y -= m_Speed * elapsedTime;

    m_Sprite.setPosition(m_Position);

    float angle = (atan2(playerPosition.y - m_Position.y, playerPosition.x - m_Position.x) * 180.0f) / (float)M_PI;

    m_Sprite.setRotation(angle);
}

bool Zombie::hit() {
    if (--m_Health <= 0)
    {
        m_Alive = false;
        m_Sprite.setTexture(TextureHolder::GetTexture(DIRECTORY+"blood.png"));
        return true;
    }

    return false;
}

bool Zombie::isAlive() { return m_Alive;}
Sprite Zombie::getSprite() { return m_Sprite;}
FloatRect Zombie::getPosition() { return m_Sprite.getGlobalBounds();}