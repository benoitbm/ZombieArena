//
// Created by benoit on 02/11/17.
//

#include "Pickup.hpp"
#include "TextureHolder.hpp"

Pickup::Pickup(int type) : m_Type(type), m_SecondsToLive(START_SECONDS_TO_LIVE), m_SecondsToWait(START_WAIT_TIME) {
    if (m_Type == 0)
    {
        m_Sprite = Sprite(TextureHolder::GetTexture(DIRECTORY+"health_pickup.png"));
        m_Value = HEALTH_START;
    }
    else
    {
        m_Sprite = Sprite(TextureHolder::GetTexture(DIRECTORY+"ammo_pickup.png"));
        m_Value = AMMO_START;
    }

    m_Sprite.setOrigin(25, 25);
}

void Pickup::setArena(IntRect arena) {
    m_Arena.left = arena.left + 50;
    m_Arena.width = arena.width - 50;
    m_Arena.top = arena.top + 50;
    m_Arena.height = arena.height - 50;

    spawn();
}

void Pickup::spawn() {
    srand((int)time(0) / (m_Type + 1));
    int x = (rand() % m_Arena.width);
    srand((int)time(0) * (m_Type + 1));
    int y = (rand() % m_Arena.height);

    m_SecondsSinceSpawn = 0;
    m_Spawned = true;

    m_Sprite.setPosition(x, y);
}

int Pickup::gotIt() {
    m_Spawned = false;
    m_SecondsSinceDespawn = 0;
    return m_Value;
}

void Pickup::update(float elapsedTime) {
    if (m_Spawned)
        m_SecondsSinceSpawn += elapsedTime;
    else
        m_SecondsSinceDespawn += elapsedTime;

    if (m_SecondsSinceSpawn > m_SecondsToLive && m_Spawned)
    {
        m_Spawned = false;
        m_SecondsSinceDespawn = 0;
    }

    if (m_SecondsSinceDespawn > m_SecondsToWait && !m_Spawned)
        spawn();
}

void Pickup::upgrade() {
    if (m_Type == 0)
        m_Value += (HEALTH_START * .5);
    else
        m_Value += AMMO_START * .5;

    m_SecondsToLive += (START_SECONDS_TO_LIVE / 10);
    m_SecondsToWait -= (START_WAIT_TIME / 10);
}